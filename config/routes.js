/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/
  'post /api/v2/contact/submit': 'ContactController.processSubmit',
  'get /api/v2/settings': 'AuthController.processListSetting',
  'post /api/v2/upload/image': 'MobileController.processImage',

  'post /api/v2/dingtoi-lite/user/registration': 'DomainUserController.processRegistration',
  'post /api/v2/dingtoi-lite/device/scanner': 'DomainDeviceController.processScanner',
  'post /api/v2/dingtoi-lite/device/rating': 'DomainDeviceController.processRating',
  'post /api/v2/dingtoi-lite/device/survey': 'DomainDeviceController.processSurvey',
  'get /api/v2/dingtoi-lite/rules': 'MobileController.processRules',

  'post /api/v2/survey-question/create': 'SurveyQuestionController.processCreate',
  'post /api/v2/survey-questions': 'SurveyQuestionController.processList',

  'post /api/v2/android/model/devices': 'MobileController.processListAndroidByModel',
  'post /api/v2/ios/model/devices': 'MobileController.processListIOSByModel',

  'get /api/v2/android/reports': 'MobileController.processReport',

  'post /api/v2/dingtoi-pro/user/login': 'AuthController.processLoginDingtoiPro',
  'post /api/v2/dingtoi-pro/user/logout': 'AuthController.processLogoutDingtoiPro',
  'post /api/v2/dingtoi-pro/user/login_by_google': 'AuthController.processLoginByGoogleDingtoiPro',
  'post /api/v2/dingtoi-pro/user/emailChecking': 'AuthController.processEmailChecking',
  'post /api/v2/dingtoi-pro/user/registration': 'AuthController.processRegisterDingtoiPro',
  'get /api/v2/dingtoi-pro/user/wallet': 'AuthController.processWalletDetail',
  'post /api/v2/dingtoi-pro/user/wallet/plus': 'AuthController.processWalletPlus',
  'post /api/v2/dingtoi-pro/user/wallet/minus': 'AuthController.processWalletMinus',
  'post /api/v2/dingtoi-pro/user/forgotPassword': 'AuthController.processForgotPassword',

  'post /api/v2/dingtoi-pro/transaction/recipients': 'TransactionController.processTransactionRecipients',
  'post /api/v2/dingtoi-pro/transaction/originators': 'TransactionController.processTransactionOriginators',
  'post /api/v2/dingtoi-pro/blacklist/check': 'DeviceController.processCheckBlacklist',
  'post /api/v2/dingtoi-pro/icloud/check': 'DeviceController.processCheckIcloud',

  'post /api/v2/dingtoi-pro/call/inbound': 'DeviceController.processCallInbound',
  'post /api/v2/dingtoi-pro/call/inbound/verification': 'DeviceController.processCallInboundVerification',

  'post /api/v2/dingtoi-pro/text/inbound': 'DeviceController.processTextInbound',
  'post /api/v2/dingtoi-pro/text/inbound/verification': 'DeviceController.processTextInboundVerification',

  'post /api/v2/dingtoi-pro/device/summaryReport': 'DeviceController.processSummaryReport',
  'post /api/v2/dingtoi-pro/device/summaryReport/physicalUpdate': 'DeviceController.processPhysicalUpdate',
  'post /api/v2/dingtoi-pro/device/scanningHistory': 'DeviceController.processScanHistory',
  'get /api/v2/dingtoi-pro/device/scanningHistory/:id': 'DeviceController.processScanHistoryDetail',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
