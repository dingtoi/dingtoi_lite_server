/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {
  ERR_BAD_REQUEST: 'Bad Request',
  ERR_NOT_FOUND: 'Not Found',
  ERR_USAGE: 'UsageError',
  ERR_ADAPTER: 'AdapterError',
  ERR_NOT_EXISTS: 'Not Exists',
  ERR_EXISTS: 'Exists',
  ERR_COMMON: 'Error Common',
  CLOUDINARY_CLOUD_NAME: 'deeucfdkq',
  CLOUDINARY_API_KEY: '232656293185281',
  CLOUDINARY_API_SECRET: '0oD503FkjXiEPx2YfkUufeFfCRo',

  /***************************************************************************
  *                                                                          *
  * Any other custom config this Sails app should use during development.    *
  *                                                                          *
  ***************************************************************************/
  // mailgunDomain: 'transactional-mail.example.com',
  // mailgunSecret: 'key-testkeyb183848139913858e8abd9a3',
  // stripeSecret: 'sk_test_Zzd814nldl91104qor5911gjald',
  // …

};
