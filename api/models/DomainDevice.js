module.exports = {
  primaryKey: 'device_token',
  attributes: {
    device_token: {
      type: 'string',
      unique: true,
      required: true,
    },
    user_token: {
      model: 'domain_user',
      required: true
    },
    user_device_type: {
      type: 'string',
      required: true,
      isIn: ['buyer', 'seller'],
    },
    price: {
      type: 'number',
      columnType: 'float',
      required: true,
    },
    currency: {
      type: 'string',
      required: true,
    },
    diamond_rating: {
      type: 'number',
      columnType: 'integer',
      allowNull: true,
    },
    physical_grading: {
      type: 'number',
      columnType: 'integer',
      allowNull: true,
    },
    device_info: {
      type: 'string',
      columnType: 'json',
      allowNull: true,
    },
    survey: {
      type: 'string',
      columnType: 'json',
      allowNull: true,
    }
  },
  identity: 'domain_device',
  tableName: 'domain_devices'
};