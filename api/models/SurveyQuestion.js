module.exports = {
  primaryKey: 'id',
  attributes: {
    id: {
      type: 'number',
      autoIncrement: true,
    },
    type: {
      type: 'string',
      columnType: 'text',
      required: true,
      isIn: ['text', 'bool', 'select', 'radio'],
    },
    message: {
      type: 'string',
      columnType: 'text',
      required: true,
    },
    list: {
      type: 'string',
      columnType: 'json',
      allowNull: true,
    },
    page: {
      type: 'number',
      columnType: 'integer',
      allowNull: true,
    },
    domain_id: {
      model: 'domain_custom',
      required: true,
    }
  },
  identity: 'survey_question',
  tableName: 'survey_questions'
};