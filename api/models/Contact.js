module.exports = {
  primaryKey: 'id',
  attributes: {
    id: {
      type: 'number',
      autoIncrement: true,
    },
    name: {
      type: 'string',
      allowNull: true
    },
    email: {
      type: 'string',
      allowNull: true
    },
    subject: {
      type: 'string',
      allowNull: true
    },
    message: {
      type: 'string',
      allowNull: true,
      columnType: 'text',
    }
  },
  identity: 'contact',
  tableName: 'contacts'
};

