module.exports = {
  primaryKey: 'id',
  attributes: {
    id: {
      type: 'number',
      autoIncrement: true,
    },
    name: {
      type: 'string',
      required: true
    },
    domain_users: {
      collection: 'domain_user',
      via: 'domain_id'
    },
    survey_questions: {
      collection: 'survey_question',
      via: 'domain_id'
    }
  },
  identity: 'domain_custom',
  tableName: 'domains'
};