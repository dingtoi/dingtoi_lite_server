module.exports = {
  primaryKey: 'token',
  attributes: {
    token: {
      type: 'string',
      unique: true,
      required: true,
    },
    ip: {
      type: 'string',
      allowNull: true
    },
    domain_id: {
      model: 'domain_custom',
      required: true
    },
    domain_devices: {
      collection: 'domain_device',
      via: 'user_token'
    }
  },
  identity: 'domain_user',
  tableName: 'domain_users'
};

