module.exports = {

  inputs: {

    token: {
      type: 'string',
      required: true
    }

  },


  fn: async function (inputs, exits) {
    var token = inputs.token;
    const tokenQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, user_id FROM oauth_access_tokens WHERE access_token=$1', [token]);
    const rows = tokenQuery.rows;
    if (rows.length == 0)
      return exits.success(false);
    return exits.success({
      user_id: rows[0].user_id,
      token: token
    });
  }

};