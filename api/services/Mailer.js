module.exports.forgotPass = function (obj) {
  sails.hooks.email.send(
    "forgotPass",
    {
      Name: obj.name
    },
    {
      to: obj.email,
      subject: "Welcome Email"
    },
    function (err) {
      console.log(err || "Mail Sent!");
    }
  )
}
