module.exports = {
  processScanner: async function (req, res) {
    const { deviceInfo, tokenDevice, tokenUser } = req.body;
    const { ERR_USAGE, ERR_ADAPTER, ERR_NOT_EXISTS } = sails.config.custom;

    if (!deviceInfo) {return res.status(422).json({ message: 'device info must provided' });}
    if (!tokenDevice) {return res.status(422).json({ message: 'token device must provided' });}
    if (!tokenUser) {return res.status(422).json({ message: 'token user must provided' });}

    const device = await domain_device.findOne({ device_token: tokenDevice, user_token: tokenUser })
      .intercept({ name: ERR_USAGE }, (err) => {
        return res.status(500).json(ERR_USAGE);
      })
      .intercept({ name: ERR_ADAPTER }, (err) => {
        return res.status(500).json(ERR_ADAPTER);
      });

    if (!device)
    {return res.status(400).json(ERR_NOT_EXISTS);}

    await domain_device.update({
      device_token: tokenDevice,
      user_token: tokenUser
    }).set({
      device_info: JSON.stringify(deviceInfo),
    });

    return res.json({ done: true });
  },

  processRating: async function (req, res) {
    const { diamondRating, physicalGrading: physicalRating, tokenDevice, tokenUser } = req.body;

    const { ERR_USAGE, ERR_ADAPTER, ERR_NOT_EXISTS } = sails.config.custom;

    if (!diamondRating) {return res.status(422).json({ message: 'diamond rating must provided' });}
    if (!physicalRating) {return res.status(422).json({ message: 'physical rating must provided' });}
    if (!tokenDevice) {return res.status(422).json({ message: 'token device must provided' });}
    if (!tokenUser) {return res.status(422).json({ message: 'token user must provided' });}

    const device = await domain_device.findOne({ device_token: tokenDevice, user_token: tokenUser })
      .intercept({ name: ERR_USAGE }, (err) => {
        return res.status(500).json(ERR_USAGE);
      })
      .intercept({ name: ERR_ADAPTER }, (err) => {
        return res.status(500).json(ERR_ADAPTER);
      });

    if (!device)
    {return res.status(400).json(ERR_NOT_EXISTS);}

    await domain_device.update({
      device_token: tokenDevice,
      user_token: tokenUser
    }).set({
      physical_grading: physicalRating,
      diamond_rating: diamondRating
    });

    return res.json({ done: true });
  },

  processSurvey: async function (req, res) {
    const { survey, tokenDevice, tokenUser } = req.body;
    const { ERR_USAGE, ERR_ADAPTER, ERR_NOT_EXISTS } = sails.config.custom;

    if (!survey) {return res.status(422).json({ message: 'survey must provided' });}
    if (!tokenDevice) {return res.status(422).json({ message: 'token device must provided' });}
    if (!tokenUser) {return res.status(422).json({ message: 'token user must provided' });}

    const device = await domain_device.findOne({ device_token: tokenDevice, user_token: tokenUser })
      .intercept({ name: ERR_USAGE }, (err) => {
        return res.status(500).json(ERR_USAGE);
      })
      .intercept({ name: ERR_ADAPTER }, (err) => {
        return res.status(500).json(ERR_ADAPTER);
      });

    if (!device)
    {return res.status(400).json(ERR_NOT_EXISTS);}

    await domain_device.update({
      device_token: tokenDevice,
      user_token: tokenUser
    }).set({
      survey: JSON.stringify(survey),
    });

    return res.json({ done: true });
  }
};

