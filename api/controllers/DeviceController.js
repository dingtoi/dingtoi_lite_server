var uuid = require('uuid');
var phoneToken = require('generate-sms-verification-code');

module.exports = {
  processTextInboundVerification: async function (req, res) {
    const { phone_number, type, code } = req.body;
    if (!phone_number) {return res.status(422).json({ message: 'phone_number must provided' });}
    if (!type) {return res.status(422).json({ type: 'type must provided' });}
    if (!code) {return res.status(422).json({ type: 'code must provided' });}
    const { ERR_BAD_REQUEST, ERR_NOT_EXISTS } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}

    const user_id = checkToken['user_id'];

    const codeSelectQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, code, type FROM device_codes WHERE alias_code=$1 AND type=$2 AND user_id=$3 AND code=$4 ORDER BY created_at DESC', [phone_number, type, user_id, code]);
    const rows = codeSelectQuery.rows;
    if (rows.length == 0)
    {return res.status(500).json({ done: ERR_NOT_EXISTS });}

    try {
      const sqlDelete = `DELETE FROM device_codes WHERE type=$1 AND alias_code=$2 AND user_id=$3`;

      await sails.getDatastore('pro').sendNativeQuery(sqlDelete, [type, phone_number, user_id]);

      return res.json({ done: true });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processTextInbound: async function (req, res) {
    const { phone_number } = req.body;
    if (!phone_number) {return res.status(422).json({ message: 'phone_number must provided' });}
    const { ERR_BAD_REQUEST } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    const idv1 = uuid.v1();
    const user_id = checkToken['user_id'];
    const date = new Date();
    const code = phoneToken(4, { type: 'number' });
    const type = 'text_inbound';

    try {

      const sqlDelete = `DELETE FROM device_codes WHERE type=$1 AND alias_code=$2 AND user_id=$3`;

      await sails.getDatastore('pro').sendNativeQuery(sqlDelete, [type, phone_number, user_id]);

      const sqlInsert = `INSERT INTO device_codes
        (id, code, type, created_at, user_id, alias_code)
        VALUES($1, $2, $3, $4, $5, $6)`;

      await sails.getDatastore('pro').sendNativeQuery(sqlInsert, [idv1, code, type, date, user_id, phone_number]);

      return res.json({ done: true, status: 'success' });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processCallInboundVerification: async function (req, res) {
    const { phone_number, type, code } = req.body;
    if (!phone_number) {return res.status(422).json({ message: 'phone_number must provided' });}
    if (!type) {return res.status(422).json({ type: 'type must provided' });}
    if (!code) {return res.status(422).json({ type: 'code must provided' });}
    const { ERR_BAD_REQUEST, ERR_NOT_EXISTS } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}

    const user_id = checkToken['user_id'];

    const codeSelectQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, code, type FROM device_codes WHERE alias_code=$1 AND type=$2 AND user_id=$3 AND code=$4 ORDER BY created_at DESC', [phone_number, type, user_id, code]);
    const rows = codeSelectQuery.rows;
    if (rows.length == 0)
    {return res.status(500).json({ done: ERR_NOT_EXISTS });}

    try {
      const sqlDelete = `DELETE FROM device_codes WHERE type=$1 AND alias_code=$2 AND user_id=$3`;

      await sails.getDatastore('pro').sendNativeQuery(sqlDelete, [type, phone_number, user_id]);

      return res.json({ done: true });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processCallInbound: async function (req, res) {
    const { phone_number } = req.body;
    if (!phone_number) {return res.status(422).json({ message: 'phone_number must provided' });}
    const { ERR_BAD_REQUEST } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    const idv1 = uuid.v1();
    const user_id = checkToken['user_id'];
    const date = new Date();
    const code = phoneToken(4, { type: 'number' });
    const type = 'call_inbound';

    try {
      const sqlDelete = `DELETE FROM device_codes WHERE type=$1 AND alias_code=$2 AND user_id=$3`;

      await sails.getDatastore('pro').sendNativeQuery(sqlDelete, [type, phone_number, user_id]);

      const sqlInsert = `INSERT INTO device_codes
        (id, code, type, created_at, user_id, alias_code)
        VALUES($1, $2, $3, $4, $5, $6)`;

      await sails.getDatastore('pro').sendNativeQuery(sqlInsert, [idv1, code, type, date, user_id, phone_number]);

      return res.json({ done: true, status: 'success' });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processPhysicalUpdate: async function(req, res){
    const { id, type, main_info } = req.body;
    if(!id){return res.status(422).json({ message: 'id must provided' });}
    if (!main_info) {return res.status(422).json({ message: 'main info must provided' });}
    if (!type) {return res.status(422).json({ message: 'type must provided' });}

    const { ERR_BAD_REQUEST, ERR_USAGE } = sails.config.custom;
    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}
    const date = new Date();
    try {
      const sqlSummaryReport = `UPDATE device_scans
        SET type=$1, main_info=$2, updated_at=$3
        WHERE id=$4`;

      await sails.getDatastore('pro').sendNativeQuery(sqlSummaryReport, [type, main_info, date, id]);

      return res.json({ done: true });
    } catch (error) {
      console.log('error '+error);
      return res.status(500).json({ done: ERR_USAGE });
    }



  },
  processSummaryReport: async function (req, res) {
    const { timestamp, main_info, type, main_url, device_id } = req.body;
    if (!timestamp) {return res.status(422).json({ message: 'timestamp must provided' });}
    if (!main_info) {return res.status(422).json({ message: 'main info must provided' });}
    if (!type) {return res.status(422).json({ message: 'type must provided' });}
    if (!main_url) {return res.status(422).json({ message: 'main URL must provided' });}
    if (!device_id) {return res.status(422).json({ message: 'device id must provided' });}
    const { ERR_BAD_REQUEST } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    const user_id = checkToken['user_id'];
    const date = new Date();

    try {
      const sqlSummaryReport = `INSERT INTO device_scans
        (id, user_id, timestamp, main_info, type, main_url, created_at, updated_at, device_id)
        VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)`;

      const idv1 = uuid.v1();

      await sails.getDatastore('pro').sendNativeQuery(sqlSummaryReport, [idv1, user_id, timestamp, main_info, type, main_url, date, date, device_id]);

      return res.json({ done: true, id: idv1 });
    } catch (error) {
      console.log('error '+error);
      return res.status(500).json({ done: ERR_USAGE });
    }

  },
  processCheckBlacklist: async function (req, res) {
    const { imei, countryCode } = req.body;
    if (!imei) {return res.status(422).json({ message: 'imei must provided' });}
    if (!countryCode) {return res.status(422).json({ message: 'country code must provided' });}
    const { ERR_BAD_REQUEST } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    return res.json({ done: true, status: 'CLEAN', type: 'success' });
    //return res.json({ done: true, status: 'LOST', type: 'error' });
    //return res.json({ done: true, status: 'ERROR', type: 'server_error' });
  },
  processCheckIcloud: async function (req, res) {
    const { ERR_BAD_REQUEST } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}


    return res.json({ done: true });
  },
  processScanHistory: async function (req, res) {
    const { unique_id } = req.body;
    if (!unique_id) {return res.status(422).json({ message: 'unique id must provided' });}
    const { ERR_BAD_REQUEST, ERR_USAGE } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    const user_id = checkToken['user_id'];

    try {
      const sqlQuery = 'SELECT id, timestamp, type FROM device_scans WHERE user_id=$1 AND device_id=$2 ORDER BY created_at DESC';
      const codeSelectQuery = await sails.getDatastore('pro').sendNativeQuery(sqlQuery, [user_id, unique_id]);
      const rows = codeSelectQuery.rows;

      return res.json({ done: true, list: rows });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },

  processScanHistoryDetail: async function (req, res) {
    const { id } = req.params;
    if (!id) {return res.status(422).json({ message: 'id must provided' });}
    const { ERR_BAD_REQUEST, ERR_USAGE } = sails.config.custom;

    if (!req.login)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
    if (!checkToken)
    {return res.status(400).json(ERR_BAD_REQUEST);}

    try {
      const sqlQuery = 'SELECT * FROM device_scans WHERE id=$1';
      const codeSelectQuery = await sails.getDatastore('pro').sendNativeQuery(sqlQuery, [id]);
      const rows = codeSelectQuery.rows;

      if (rows.length == 0)
      {return res.status(500).json({ done: ERR_NOT_EXISTS });}

      return res.json({ done: true, detail: rows[0] });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  }
};
