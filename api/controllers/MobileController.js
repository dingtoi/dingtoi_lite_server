
var fs = require('fs');
var path = require('path');

var cloudinary = require('cloudinary').v2;
var moment = require('moment');

cloudinary.config({
  cloud_name: sails.config.custom.CLOUDINARY_CLOUD_NAME,
  api_key: sails.config.custom.CLOUDINARY_API_KEY,
  api_secret: sails.config.custom.CLOUDINARY_API_SECRET,
});

module.exports = {
  processImage: function (req, res){
    const { ERR_USAGE } = sails.config.custom;
    req.file('image').upload((err, uploadedFiles) => {
      if (err) {
        return res.status(500).json({ done: ERR_USAGE });
      } else {
        if (uploadedFiles.length > 0) {
          cloudinary.uploader.upload(uploadedFiles[0].fd, {'folder': 'device-scan'}, (error, result) => {
            if(error){
              return res.status(500).json({ done: error });
            }else
            {
              return res.json({ done: true, url: result.url });
            }
          });
        } else {
          return res.status(500).json({ done: ERR_USAGE });
        }
      }
    });
  },

  processRules: function (req, res) {
    const { ERR_USAGE } = sails.config.custom;
    try {
      let list = {
        total: 50,
        touchscreen: 30,
        bluetooth: 5,
        wifi: 5,
        finger: 2,
        camera: 15,
        flash: 15,
        storage: 5,
        processor: 5,
        released: 5,
        faceID: 2,
      };
      return res.json({ done: true, list });
    } catch (error) {
      return res.status(500).json(ERR_USAGE);
    }
  },
  processListAndroidByModel: function (req, res) {
    const { model } = req.body;
    const { ERR_USAGE } = sails.config.custom;
    if (!model) {return res.status(422).json({ message: 'Model must provided' });}

    try {
      const file = path.resolve('assets', 'samsung.json');

      let samsungList = fs.readFileSync(file, 'utf8');
      samsungList = JSON.parse(samsungList);

      let arr = [];
      for (var i = 0; i < samsungList.length; i++) {
        let item = samsungList[i];
        if (item.modelVersion.toLowerCase().includes(model.toLowerCase())) {
          arr.push(item);
        }
      }
      return res.json({ done: true, list: arr });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processListIOSByModel: function (req, res) {
    const { model } = req.body;
    const { ERR_USAGE } = sails.config.custom;
    if (!model) {return res.status(422).json({ message: 'Model must provided' });}

    try {
      const file = path.resolve('assets', 'apple.json');

      let appleList = fs.readFileSync(file, 'utf8');
      appleList = JSON.parse(appleList);

      let arr = [];
      for (var i = 0; i < appleList.length; i++) {
        let item = appleList[i];
        if (item.model.toLowerCase().includes(model.toLowerCase())) {
          arr.push(item);
        }
      }
      return res.json({ done: true, list: arr });
    } catch (error) {
      return res.status(500).json(ERR_USAGE);
    }
  },
  processReport: async function (req, res) {
    const rows = await domain_device.find({
      select: ['createdAt', 'user_device_type', 'price', 'currency', 'diamond_rating', 'physical_grading', 'device_info', 'survey'],
      where: { createdAt: { '>': new Date('2020-07-22').getTime() } }
    }).sort('createdAt DESC');


    var newRows = [];
    for (var i = 0; i < rows.length; i++) {
      rows[i].createdAt = moment(rows[i].createdAt).format('YYYY-MM-DD HH:mm:ss');
      delete rows[i].createdAt;
      delete rows[i].device_token;
      rows[i].device_info = JSON.parse(JSON.parse(rows[i].device_info));
      var checked = false;

      if (rows[i].device_info) {
        if (rows[i].device_info.model.includes('iPhone') || rows[i].device_info.model.includes('iPad')) {
          delete rows[i].device_info.scannerPoint;
          rows[i].device_info.time = moment(rows[i].device_info.time).format('YYYY-MM-DD HH:mm:ss');

          var total = 50;

          if (rows[i].device_info.touchscreen == 'n') {
            total -= 30;
          }
          if (rows[i].device_info.camera == 'n') {
            total -= 15;
          }

          if (rows[i].device_info.flash == 'n') {
            total -= 15;
          }
          if (rows[i].device_info.wifi == 'n') {
            total -= 5;
          }
          if (rows[i].device_info.bluetooth == 'n') {
            total -= 5;
          }
          if (rows[i].device_info.finger == 'n') {
            total -= 2;
          }
          if (rows[i].device_info.faceid == 'n') {
            total -= 2;
          }

          if (total < 0) {
            total = 0;
          }

          rows[i].scanner_point = total;
          if (rows[i].device_info.model == 'SM-G950W') {rows[i].device_info.model = 'Samsung Galaxy S8';}
          else if (rows[i].device_info.model == 'CPH1825') {rows[i].device_info.model = 'Oppo F9';}
          checked = true;
        }

      } else {
        checked = true;
      }
      if (checked) {
        rows[i].survey = JSON.parse(JSON.parse(rows[i].survey));
        if (rows[i].survey) {
          delete rows[i].survey[0].id;
          rows[i].survey[0].name = 'Did Dingtoi make the transaction easier and was it useful ?';
          delete rows[i].survey[1].id;
          rows[i].survey[1].name = 'Do you feel that Dingtoi helped you sell or buy your phone ?';
          delete rows[i].survey[2].id;
          rows[i].survey[2].name = 'Was the Dingtoi Rating accurate for the overall condition of the phone ?';
          delete rows[i].survey[3].id;
          rows[i].survey[3].name = 'Would you highly recommend Dingtoi to someone ?';
          delete rows[i].survey[4].id;
          rows[i].survey[4].name = 'Your Age ?';
          //rows[i].survey[4].message = '21-39 age';
          delete rows[i].survey[5].id;
          rows[i].survey[5].name = 'Your Sex ?';
        }

        newRows.push(rows[i]);
      }

    }
    console.log('rows', newRows.length);

    res.json(newRows);
  }
};

