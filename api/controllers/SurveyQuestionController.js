module.exports = {
  processCreate: async function (req, res) {
    const { message, domain, type } = req.body;
    const { ERR_USAGE, ERR_ADAPTER } = sails.config.custom;

    if (!message) return res.status(422).json({ message: 'message must provided' });
    if (!domain) return res.status(422).json({ message: 'domain must provided' });
    if (!type) return res.status(422).json({ message: 'type must provided' });

    var list = [
      { message: 'Male', value: 'm' },
      { message: 'Female', value: 'f' },
      { message: 'Unspecified', value: 'u' },
    ];

    await survey_question.create({
      message, domain_id: domain, type, page: 2, list: JSON.stringify(list),
    })
      .intercept({ name: ERR_USAGE }, (err) => {
        return ERR_USAGE;
      })
      .intercept({ name: ERR_ADAPTER }, (err) => {
        return ERR_ADAPTER;
      });

    return res.json({ done: true });
  },
  processList: async function (req, res) {
    const { limit, skip, domain } = req.body;
    const { ERR_USAGE, ERR_ADAPTER } = sails.config.custom;

    if (!limit) return res.status(422).json({ message: 'limit must provided' });
    if (!skip) return res.status(422).json({ message: 'skip must provided' });


    const questions = await survey_question.find({ where: { domain_id: domain }, limit, skip })
      .intercept({ name: ERR_USAGE }, (err) => {
        return ERR_USAGE;
      })
      .intercept({ name: ERR_ADAPTER }, (err) => {
        return ERR_ADAPTER;
      });

    const total = await survey_question.count({ where: { domain_id: domain } });

    return res.json({ done: true, list: questions, total });
  }
};

