module.exports = {
  processRegistration: async function (req, res) {
    const { tokenUser, type, price, tokenDevice, domain, currency } = req.body;
    const { ERR_BAD_REQUEST, ERR_NOT_FOUND } = sails.config.custom;

    if (!tokenUser) {return res.status(422).json({ message: 'token user must provided' });}
    if (!type) {return res.status(422).json({ message: 'type must provided' });}
    if (!price) {return res.status(422).json({ message: 'price must provided' });}
    if (!tokenDevice) {return res.status(422).json({ message: 'token device must provided' });}
    if (!domain) {return res.status(422).json({ message: 'domain must provided' });}
    if (!currency) {return res.status(422).json({ message: 'currency must provided' });}

    const result = await sails.getDatastore()
      .transaction(async (db) => {
        const user = await domain_user.findOne({ token: tokenUser }).usingConnection(db);
        if (!user) {
          await domain_user.create({
            token: tokenUser,
            domain_id: domain,
          }).usingConnection(db);

          await domain_device.create({
            device_token: tokenDevice,
            user_token: tokenUser,
            user_device_type: type,
            price: price,
            currency: currency
          }).usingConnection(db);
        } else {
          await domain_device.create({
            device_token: tokenDevice,
            user_token: tokenUser,
            user_device_type: type,
            price: price,
            currency: currency
          }).usingConnection(db);
        }
      })
      .intercept('E_INSUFFICIENT_FUNDS', () => ERR_BAD_REQUEST)
      .intercept('E_NO_SUCH_RECIPIENT', () => ERR_NOT_FOUND);

    if (result == ERR_BAD_REQUEST) {return res.status(500).json(ERR_BAD_REQUEST);}
    else if (result == ERR_NOT_FOUND) {return res.status(404).json(ERR_NOT_FOUND);}
    else
    {return res.json({ done: true });}
  }
};
