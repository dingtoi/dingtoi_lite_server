var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var uuid = require('uuid');

module.exports = {
  processListSetting: async function (req, res) {
    const { ERR_USAGE, ERR_BAD_REQUEST } = sails.config.custom;
    if (!req.login)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}
    try {
      var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
      if (!checkToken)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const settingQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT * FROM settings');
      const rows = settingQuery.rows;

      return res.json({ done: true, list: rows });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processWalletDetail: async function (req, res) {
    const { ERR_USAGE, ERR_BAD_REQUEST } = sails.config.custom;
    if (!req.login)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}
    try {
      var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
      if (!checkToken)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const user_id = checkToken['user_id'];
      const userQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, email, password, wallet FROM users WHERE id=$1', [user_id]);
      const rows = userQuery.rows;
      if (rows.length == 0)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const wallet = rows[0].wallet;
      return res.json({ done: true, wallet });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processWalletPlus: async function (req, res) {
    const { ERR_USAGE, ERR_BAD_REQUEST } = sails.config.custom;
    const { money } = req.body;
    if (!money) {return res.status(422).json({ message: 'money must provided' });}
    if (!req.login)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}
    try {
      var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
      if (!checkToken)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const user_id = checkToken['user_id'];
      const userQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, email, password, wallet FROM users WHERE id=$1', [user_id]);
      const rows = userQuery.rows;
      if (rows.length == 0)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const wallet = rows[0].wallet;
      var newWallet = wallet ? wallet : 0;
      newWallet = newWallet + money;
      await sails.getDatastore('pro').sendNativeQuery('UPDATE users SET wallet=$1 WHERE id=$2', [newWallet, user_id]);
      return res.json({ done: true, wallet: newWallet });
    } catch (error) {
      console.log('error', error);
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processWalletMinus: async function (req, res) {
    const { ERR_USAGE, ERR_BAD_REQUEST } = sails.config.custom;
    const { money } = req.body;
    if (!money) {return res.status(422).json({ message: 'money must provided' });}
    if (!req.login)
    {return res.status(400).json({ done: ERR_BAD_REQUEST });}
    try {
      var checkToken = await sails.helpers.proCheckToken.with({ token: req.token });
      if (!checkToken)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const user_id = checkToken['user_id'];
      const userQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, email, password, wallet FROM users WHERE id=$1', [user_id]);
      const rows = userQuery.rows;
      if (rows.length == 0)
      {return res.status(400).json({ done: ERR_BAD_REQUEST });}
      const wallet = rows[0].wallet;
      var newWallet = wallet ? wallet : 0;
      newWallet = newWallet - money;
      await sails.getDatastore('pro').sendNativeQuery('UPDATE users SET wallet=$1 WHERE id=$2', [newWallet, user_id]);
      return res.json({ done: true, wallet: newWallet });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processLoginByGoogleDingtoiPro: async function (req, res) {
    const { email } = req.body;
    if (!email) {return res.status(422).json({ message: 'email must provided' });}
    const { ERR_USAGE, ERR_BAD_REQUEST, ERR_NOT_FOUND } = sails.config.custom;


    try {
      const userQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, email, password, wallet FROM users WHERE email=$1', [email]);
      const rows = userQuery.rows;
      if (rows.length == 0) {
        const userId = uuid.v1();
        const pass = bcrypt.hashSync(userId, 10);
        const date = new Date();
        var token = jwt.sign({ id: userId, email }, email);

        const result = await sails.getDatastore('pro')
          .transaction(async (db) => {
            const sqlInsertIntoUser = 'INSERT INTO users(id, active, email, password, created_at, updated_at, active_code, client_id, wallet, user_social_id) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)';
            await sails.getDatastore('pro').sendNativeQuery(sqlInsertIntoUser, [userId, false, email, pass, date, date, userId, 4, 0, 3]).usingConnection(db);

            const sqlInsertIntoAccessToken = `INSERT INTO oauth_access_tokens
            (access_token, scope, created_at, updated_at, client_id, user_id)
            VALUES($1, $2, $3, $4, $5, $6)`;

            await sails.getDatastore('pro').sendNativeQuery(sqlInsertIntoAccessToken, [token, true, date, date, 4, userId]).usingConnection(db);
            return {};

          })
          .intercept('E_INSUFFICIENT_FUNDS', () => ERR_BAD_REQUEST)
          .intercept('E_NO_SUCH_RECIPIENT', () => ERR_NOT_FOUND);

        if (result == ERR_BAD_REQUEST) {return res.status(500).json(ERR_BAD_REQUEST);}
        else if (result == ERR_NOT_FOUND) {return res.status(404).json(ERR_NOT_FOUND);}
        else
        {return res.json({ done: true, user: { token, id: userId, email, wallet: null } });}
      } else {
        const userId = rows[0].id;
        var token = jwt.sign({ id: userId, email }, email);

        const wallet = rows[0].wallet;
        const date = new Date();
        const sqlInsertIntoAccessToken = `INSERT INTO oauth_access_tokens
            (access_token, scope, created_at, updated_at, client_id, user_id)
            VALUES($1, $2, $3, $4, $5, $6)`;

        await sails.getDatastore('pro').sendNativeQuery(sqlInsertIntoAccessToken, [token, true, date, date, 4, userId]);


        return res.json({ done: true, user: { token, id: userId, email, wallet } });
      }
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processForgotPassword: async function (req, res) {
    const { email } = req.body;
    if (!email) {return res.status(422).json({ message: 'email must provided' });}
    const { ERR_USAGE, ERR_NOT_EXISTS } = sails.config.custom;

    try {
      const userQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id FROM users WHERE email=$1', [email]);
      const rows = userQuery.rows;
      if (rows.length == 0)
      {return res.status(400).json({ done: ERR_NOT_EXISTS });}

      Mailer.forgotPass({ email: 'buivuongdhmo@gmail.com' });

      return res.json({ done: true });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processLoginDingtoiPro: async function (req, res) {
    const { email, password } = req.body;
    if (!email) {return res.status(422).json({ message: 'email must provided' });}
    if (!password) {return res.status(422).json({ message: 'password must provided' });}
    const { ERR_USAGE, ERR_NOT_EXISTS, ERR_COMMON } = sails.config.custom;

    try {
      const userQuery = await sails.getDatastore('pro').sendNativeQuery('SELECT id, email, password, wallet FROM users WHERE email=$1', [email]);
      const rows = userQuery.rows;
      if (rows.length == 0)
      {return res.status(400).json({ done: ERR_NOT_EXISTS });}
      const userPassword = rows[0].password;
      const userId = rows[0].id;
      const wallet = rows[0].wallet;
      var token = jwt.sign({ id: userId, email }, email);
      const isSame = bcrypt.compareSync(password, userPassword);

      if (!isSame)
      {return res.status(400).json({ done: ERR_COMMON });}

      const date = new Date();
      const sqlInsertIntoAccessToken = `INSERT INTO oauth_access_tokens
        (access_token, scope, created_at, updated_at, client_id, user_id)
        VALUES($1, $2, $3, $4, $5, $6)`;

      await sails.getDatastore('pro').sendNativeQuery(sqlInsertIntoAccessToken, [token, true, date, date, 4, userId]);


      return res.json({ done: true, user: { token, id: userId, email, wallet } });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }
  },
  processRegisterDingtoiPro: async function (req, res) {
    const { email, password } = req.body;
    if (!email) {return res.status(422).json({ message: 'email must provided' });}
    if (!password) {return res.status(422).json({ message: 'password must provided' });}
    const { ERR_USAGE, ERR_EXISTS } = sails.config.custom;

    try {
      const sqlExistsUser = 'SELECT id FROM users WHERE email=$1';
      const userQuery = await sails.getDatastore('pro').sendNativeQuery(sqlExistsUser, [email]);
      const userRows = userQuery.rows;
      if (userRows.length > 0)
      {return res.status(500).json({ done: ERR_EXISTS });}
      const idv1 = uuid.v1();
      const pass = bcrypt.hashSync(password, 10);
      const sqlInsertIntoUser = 'INSERT INTO users(id, active, email, password, created_at, updated_at, active_code, client_id, wallet, user_social_id) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)';
      const date = new Date();
      await sails.getDatastore('pro').sendNativeQuery(sqlInsertIntoUser, [idv1, false, email, pass, date, date, idv1, 4, 0, 1]);
      return res.json({ done: true });
    } catch (error) {
      return res.status(500).json({ done: ERR_USAGE });
    }

  },
  processLogoutDingtoiPro: async function (req, res, next) {
    if (!req.login)
    {return res.json({ done: true });}

    const token = req.token;

    try {
      const sqlDeleteToken = 'DELETE FROM oauth_access_tokens WHERE access_token=$1';
      await sails.getDatastore('pro').sendNativeQuery(sqlDeleteToken, [token]);
      return res.json({ done: true });
    } catch (error) {
      return res.json({ done: true });
    }
  },
  processEmailChecking: async function (req, res, next) {
    const { ERR_USAGE } = sails.config.custom;

    const { email, password } = req.body;
    if (!email) {return res.status(422).json({ message: 'email must provided' });}

    try {
      const sqlUser = 'SELECT id, email FROM users WHERE email=$1';
      const query = await sails.getDatastore('pro').sendNativeQuery(sqlUser, [email]);
      const rows = query.rows;
      if (rows.length == 0)
      {return res.json({ done: false });}
      return res.json({ done: true });
    } catch (error) {
      return res.status(500).json(ERR_USAGE);
    }
  }
};
