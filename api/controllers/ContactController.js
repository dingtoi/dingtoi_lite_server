module.exports = {
  processSubmit: async function (req, res) {
    const { name, email, subject, message } = req.body;
    const { ERR_USAGE, ERR_ADAPTER } = sails.config.custom;

    await contact.create({
      name, email, subject, message
    })
      .intercept({ name: ERR_USAGE }, (err) => {
        return ERR_USAGE;
      })
      .intercept({ name: ERR_ADAPTER }, (err) => {
        return ERR_ADAPTER;
      });

    return res.json({ done: true });
  },
};

