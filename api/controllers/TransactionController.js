var fs = require('fs');
var path = require('path');

module.exports = {
  processTransactionOriginators: async function (req, res) {
    const { userId } = req.body;
    const { ERR_USAGE } = sails.config.custom;
    if (!userId) return res.status(422).json({ message: 'User Id must provided' });
    try {
      const sqlProposalList = `SELECT transactions.id AS id, 
      transactions.money AS money, 
        transactions."finalMoney" AS "finalMoney", 
        transactions.status AS "status",
        transactions."transactionCode" AS "transactionCode",
        transactions."createdAt" as "createdAt",
        proposal_exchanges.device_id AS "exchangeDeviceId",
        proposal_exchanges.imei AS "exchangeImei",
        proposal_exchanges.model_name AS "exchangeModelName",
        proposal_exchanges.image_url AS "exchangeImageUrl"
      FROM transactions    
      LEFT JOIN
      (
        SELECT proposal_exchanges.id AS id,
        proposal_exchanges.seller_id AS seller_id,
        carts."availableDeviceId" AS device_id,
        carts.imei AS imei,
        carts.model_name AS model_name,
        carts.image_url AS image_url
        FROM proposal_exchanges
        INNER JOIN 
        (
          SELECT carts.id AS id,
          carts."availableDeviceId" AS "availableDeviceId",
          available_devices.imei AS imei,
          available_devices.model_name AS model_name,
          available_devices.image_url AS image_url
          FROM carts
          INNER JOIN (
            SELECT available_devices.id,
            devices.imei AS imei,
            devices.model_name AS model_name,
            devices.image_url AS image_url
            FROM available_devices
            INNER JOIN (
              SELECT devices.id AS id,
                imeis.imei AS imei,
                imeis.model_name AS model_name,
                device_images.thumbnail_url AS image_url
              FROM devices
              LEFT JOIN device_images
              ON device_images."deviceId" = devices.id AND device_images.main = 1
              INNER JOIN 
              (
                SELECT imeis.id AS id,
                imeis.imei AS imei,
                model_details.name AS model_name
                FROM imeis
                INNER JOIN (
                  SELECT model_details.id AS id,
                  models.name AS name
                  FROM model_details
                  INNER JOIN models
                  ON model_details."modelId" = models.id
                ) AS model_details
                ON imeis."modelDetailId" = model_details.id
              )
              AS imeis
              ON imeis.id = devices."imeiId"
            ) AS devices
            ON available_devices."deviceId" = devices.id
          ) AS available_devices
          ON carts."availableDeviceId" = available_devices.id
        )
        AS carts
        ON proposal_exchanges.cart_id = carts.id
      )
      AS proposal_exchanges 
      ON transactions."proposalExchangeId" = proposal_exchanges.id AND proposal_exchanges.seller_id=$1
  
      WHERE transactions."deletedAt" IS NULL
      `;

      const proposalQuery = await sails.getDatastore('pro').sendNativeQuery(sqlProposalList, [userId]);
      const proposalRows = proposalQuery.rows;
      return res.json(proposalRows);
    } catch (error) {
      return res.status(500).json(ERR_USAGE);
    }
  },
  processTransactionRecipients: async function (req, res) {
    const { userId } = req.body;
    const { ERR_USAGE } = sails.config.custom;
    if (!userId) return res.status(422).json({ message: 'User Id must provided' });

    try {
      const sqlProposalList = `SELECT transactions.id AS id, 
      transactions.money AS money, 
        transactions."finalMoney" AS "finalMoney", 
        transactions.status AS "status",
        transactions."transactionCode" AS "transactionCode",
        transactions."createdAt" as "createdAt",
        proposal_sales.device_id AS "saleDeviceId",
        proposal_sales.imei AS "saleImei",
        proposal_sales.model_name AS "saleModelName",
        proposal_sales.image_url AS "saleImageUrl",
        proposal_exchanges.device_id AS "exchangeDeviceId",
        proposal_exchanges.imei AS "exchangeImei",
        proposal_exchanges.model_name AS "exchangeModelName",
        proposal_exchanges.image_url AS "exchangeImageUrl"
      FROM transactions
  
      LEFT JOIN
      (
        SELECT proposal_sales.id AS id,
        proposal_sales.buyer_id AS buyer_id,
        carts."availableDeviceId" AS device_id,
        carts.imei AS imei,
        carts.model_name AS model_name,
        carts.image_url AS image_url
        FROM proposal_sales
        INNER JOIN 
        (
          SELECT carts.id AS id,
          carts."availableDeviceId" AS "availableDeviceId",
          available_devices.imei AS imei,
          available_devices.model_name AS model_name,
          available_devices.image_url AS image_url
          FROM carts
          INNER JOIN (
            SELECT available_devices.id,
            devices.imei AS imei,
            devices.model_name AS model_name,
            devices.image_url AS image_url
            FROM available_devices
            INNER JOIN (
              SELECT devices.id AS id,
                imeis.imei AS imei,
                imeis.model_name AS model_name,
                device_images.thumbnail_url AS image_url
              FROM devices
              LEFT JOIN device_images
              ON device_images."deviceId" = devices.id AND device_images.main = 1
              INNER JOIN
              (
                SELECT imeis.id AS id,
                imeis.imei AS imei,
                model_details.name AS model_name
                FROM imeis
                INNER JOIN (
                  SELECT model_details.id AS id,
                  models.name AS name
                  FROM model_details
                  INNER JOIN models
                  ON model_details."modelId" = models.id
                ) AS model_details
                ON imeis."modelDetailId" = model_details.id
              )
              AS imeis
              ON imeis.id = devices."imeiId"
            ) AS devices
            ON available_devices."deviceId" = devices.id
          ) AS available_devices
          ON carts."availableDeviceId" = available_devices.id
        )
        AS carts
        ON proposal_sales.cart_id = carts.id
      )
      AS proposal_sales 
      ON transactions."proposalSaleId" = proposal_sales.id
      
      LEFT JOIN
      (
        SELECT proposal_exchanges.id AS id,
        proposal_exchanges.buyer_id AS buyer_id,
        carts."availableDeviceId" AS device_id,
        carts.imei AS imei,
        carts.model_name AS model_name,
        carts.image_url AS image_url
        FROM proposal_exchanges
        INNER JOIN 
        (
          SELECT carts.id AS id,
          carts."availableDeviceId" AS "availableDeviceId",
          available_devices.imei AS imei,
          available_devices.model_name AS model_name,
          available_devices.image_url AS image_url
          FROM carts
          INNER JOIN (
            SELECT available_devices.id,
            devices.imei AS imei,
            devices.model_name AS model_name,
            devices.image_url AS image_url
            FROM available_devices
            INNER JOIN (
              SELECT devices.id AS id,
                imeis.imei AS imei,
                imeis.model_name AS model_name,
                device_images.thumbnail_url AS image_url
              FROM devices
              LEFT JOIN device_images
              ON device_images."deviceId" = devices.id AND device_images.main = 1
              INNER JOIN 
              (
                SELECT imeis.id AS id,
                imeis.imei AS imei,
                model_details.name AS model_name
                FROM imeis
                INNER JOIN (
                  SELECT model_details.id AS id,
                  models.name AS name
                  FROM model_details
                  INNER JOIN models
                  ON model_details."modelId" = models.id
                ) AS model_details
                ON imeis."modelDetailId" = model_details.id
              )
              AS imeis
              ON imeis.id = devices."imeiId"
            ) AS devices
            ON available_devices."deviceId" = devices.id
          ) AS available_devices
          ON carts."availableDeviceId" = available_devices.id
        )
        AS carts
        ON proposal_exchanges.cart_id = carts.id
      )
      AS proposal_exchanges 
      ON transactions."proposalExchangeId" = proposal_exchanges.id
  
      WHERE (transactions."deletedAt" IS NULL AND proposal_sales.buyer_id=$1)
      OR (transactions."deletedAt" IS NULL AND proposal_exchanges.buyer_id=$1)
      `;

      const proposalQuery = await sails.getDatastore('pro').sendNativeQuery(sqlProposalList, [userId]);
      const proposalRows = proposalQuery.rows;

      const sqlExchangeList = `
        SELECT transactions.id AS id, 
        transactions.money AS money, 
          transactions."finalMoney" AS "finalMoney", 
          transactions.status AS "status",
          transactions."transactionCode" AS "transactionCode",
          transactions."createdAt" as "createdAt",
          proposal_exchanges.proposal_exchange_device AS "proposalExchangeDevice",
          proposal_exchanges.imei AS "exchangeImei",
          proposal_exchanges.model_name AS "exchangeModelName",
          proposal_exchanges.image_url AS "exchangeImageUrl"
        FROM transactions
        LEFT JOIN
        (
          SELECT proposal_exchanges.id AS id,
          proposal_exchanges.seller_id AS seller_id,
          devices.imei AS imei,
          devices.model_name AS model_name,
          devices.image_url AS image_url,
          unnest(proposal_exchanges.proposal_exchange_devices) AS proposal_exchange_device
          FROM proposal_exchanges
          INNER JOIN (
            SELECT devices.id AS id,
              imeis.imei AS imei,
              imeis.model_name AS model_name,
              device_images.thumbnail_url AS image_url
            FROM devices
            LEFT JOIN device_images
            ON device_images."deviceId" = devices.id AND device_images.main = 1
            INNER JOIN 
            (
              SELECT imeis.id AS id,
              imeis.imei AS imei,
              model_details.name AS model_name
              FROM imeis
              INNER JOIN (
                SELECT model_details.id AS id,
                models.name AS name
                FROM model_details
                INNER JOIN models
                ON model_details."modelId" = models.id
              ) AS model_details
              ON imeis."modelDetailId" = model_details.id
            )
            AS imeis
            ON imeis.id = devices."imeiId"
          ) AS devices
          ON  devices.id = ANY (proposal_exchanges.proposal_exchange_devices)
        )
        AS proposal_exchanges 
        ON transactions."proposalExchangeId" = proposal_exchanges.id
        WHERE transactions."deletedAt" IS NULL AND proposal_exchanges.seller_id=$1
      `;

      const exchangeQuery = await sails.getDatastore('pro').sendNativeQuery(sqlExchangeList, [userId]);
      const exchangeRows = exchangeQuery.rows;

      const finalRows = proposalRows.concat(exchangeRows);

      return res.json(finalRows);
    } catch (error) {
      return res.status(500).json(error);
    }

  }
};